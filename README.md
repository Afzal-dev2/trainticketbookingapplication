# Train Ticket Booking Application
    By: [Mohaammed Afzal -  WDGET2024074]

## Table of Contents
- [Pre-Requisites](#pre-requisites)
- [Features](#features)
- [Java Classes](#java-classes)
    - [HibernateUtil.java](#hibernateutiljava)
    - [Train.java](#trainjava)
    - [Customer.java](#customerjava)
    - [Booking.java](#bookingjava)
    - [MasterList.java](#masterlistjava)
    - [DAO Implementation](#dao-implementation)
        - [TrainDAOImpl.java](#traindaoimpljava)
        - [CustomerDAOImpl.java](#customerdaoimpljava)
        - [BookingDAOImpl.java](#bookingdaoimpljava)
    - [Service Implementation](#service-implementation)
        - [TrainServiceImpl.java](#trainserviceimpljava)
        - [CustomerServiceImpl.java](#customerserviceimpljava)
        - [BookingServiceImpl.java](#bookingserviceimpljava)
    - [Servlets](#servlets)
        - [CustomerRegistrationServlet.java](#customerregistrationservletjava)
        - [LoginServlet.java](#loginservletjava)
        - [DashboardManagerServlet.java](#dashboardmanagerservletjava)
        - [TicketBookingServlet.java](#ticketbookingservletjava)
        - [ViewTicketServlet.java](#viewticketservletjava)
        - [TicketCancellationServlet.java](#ticketcancellationservletjava)

## Pre-Requisites
- Java Development Kit (JDK)
- Apache Tomcat
- MySQL or another supported database
- Hibernate

## Features
- Servlet-based web application for Booking and cancellation of train tickets
- Integration with Hibernate for ORM
- User registration and login functionality
- View and manage booked tickets

## Java Classes

### HibernateUtil.java
- **Functionality**: Initializes the Hibernate `SessionFactory` and provides access to it.
- **Usage**: Utilized by other classes to obtain a Hibernate `Session` for database operations.

### Train.java
- **Functionality**: Represents a train entity with attributes such as ID, source, destination, fare, etc.
- **Usage**: Used to model train data in the application.

### Customer.java
- **Functionality**: Represents a customer entity with attributes such as name, email, password, wallet balance, etc.
- **Usage**: Used for user registration, login, and customer-related operations.

### Booking.java
- **Functionality**: Represents a booking entity with attributes such as PNR, customer ID, train details, journey date, etc.
- **Usage**: Used to manage train ticket bookings.

### MasterList.java
- **Functionality**: Represents a master list entity containing details of passengers associated with a booking.
- **Usage**: Used to store and manage passenger details for bookings.

### DAO Implementation

#### TrainDAOImpl.java
- **Functionality**: Implements the TrainDAO interface for train-related database operations using Hibernate.
- **Usage**: Provides methods to interact with the train database table.

#### CustomerDAOImpl.java
- **Functionality**: Implements the CustomerDAO interface for customer-related database operations using Hibernate.
- **Usage**: Provides methods to interact with the customer database table.

#### BookingDAOImpl.java
- **Functionality**: Implements the BookingDAO interface for booking-related database operations using Hibernate.
- **Usage**: Provides methods to interact with the booking database table.

### Service Implementation

#### TrainServiceImpl.java
- **Functionality**: Implements the TrainService interface for train-related business logic.
- **Usage**: Provides methods to perform operations on trains.

#### CustomerServiceImpl.java
- **Functionality**: Implements the CustomerService interface for customer-related business logic.
- **Usage**: Provides methods to perform operations on customers.

#### BookingServiceImpl.java
- **Functionality**: Implements the BookingService interface for booking-related business logic.
- **Usage**: Provides methods to perform operations on bookings.

### Servlets

#### CustomerRegistrationServlet.java
- **Functionality**: Handles customer registration by receiving form data and storing it in the database.
- **Usage**: Servlet for user registration.

#### LoginServlet.java
- **Functionality**: Handles user login by validating credentials against the database.
- **Usage**: Servlet for user login.

#### DashboardManagerServlet.java
- **Functionality**: Redirects users based on the provided ID to different pages (book, view, cancel).
- **Usage**: Servlet for managing user dashboard navigation.

#### TicketBookingServlet.java
- **Functionality**: Handles the booking of train tickets by processing form data and updating the database.
- **Usage**: Servlet for booking train tickets.

#### ViewTicketServlet.java
- **Functionality**: Retrieves booking details based on the provided PNR and displays them to the user.
- **Usage**: Servlet for viewing booked train tickets.

#### TicketCancellationServlet.java
- **Functionality**: Handles the cancellation of booked train tickets and updates the database accordingly.
- **Usage**: Servlet for canceling booked train tickets.


## Screenshots

![sc1](screenshots/sc1.png)
![sc2](screenshots/sc2.png)
![sc3](screenshots/sc3.png)
![sc4](screenshots/sc4.png)
![sc5](screenshots/sc5.png)
![sc6](screenshots/sc6.png)
![sc7](screenshots/sc7.png)
![sc8](screenshots/sc8.png)
![sc9](screenshots/sc9.png)
![sc10](screenshots/sc10.png)
![sc11](screenshots/sc11.png)


- 