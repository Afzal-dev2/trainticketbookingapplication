<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>View Ticket</title>
    <link rel="stylesheet" href="CSS/view_ticket_style.css">
</head>
<body>
<header>
    <h1>Train ticket booking application</h1>
</header>
<h1 id="title">View Ticket</h1>
<form action="view" method="get">
    <label for="pnr">Enter PNR Number:</label>
    <input type="text" id="pnr" name="pnr" required>
    <button type="submit">View Booking Details</button>
</form>
<hr>
<c:if test="${not empty bookingDetails}">
    <!-- Display booking details if available -->

       <table class="details-table">
           <tr>
               <th>PNR Number:</th>
               <td>${bookingDetails.pnr}</td>
           </tr>
           <tr>
               <th>Customer Name:</th>
               <td>${customerData.name}</td>
           </tr>
           <tr>
               <th>Train Name:</th>
               <td>${bookingDetails.train.name}</td>
           </tr>
           <tr>
               <th>Journey Date:</th>
               <td>${bookingDetails.journeyDate}</td>
           </tr>
           <tr>
               <th>Berth Preference:</th>
               <td>${customerData.berthPreference}</td>
           </tr>
           <tr>
               <th>Number of Passengers:</th>
               <td>${bookingDetails.numberOfPassengers}</td>
           </tr>
       </table>

    <!-- Display passenger names if available -->
    <c:if test="${not empty bookingPassengers}">
        <table class="passenger-table">
            <tr>
                <th>Co-Passenger Name</th>
                <th>Co-Passenger Age</th>
                <th>Co-Passenger Berth Preference</th>
            </tr>
            <c:forEach var="passenger" items="${bookingPassengers}">
                <tr>
                    <td>${passenger.name}</td>
                    <td>${passenger.age}</td>
                    <td>${passenger.berthPreference}</td>
                </tr>
            </c:forEach>
        </table>
    </c:if>

    <!-- Display error message if no passengers found -->
    <c:if test="${empty bookingPassengers}">
        <p id="errorMsg">No booking & passengers found.</p>
    </c:if>
</c:if>

<!-- Display error message if booking details not found -->
<c:if test="${empty bookingDetails && not empty errorMessage}">
    <p id="errorMsg">${errorMessage}</p>
</c:if>
</body>
</html>
