<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ticket Cancellation</title>
    <link rel="stylesheet" href="CSS/cancel_ticket.css">
</head>

<body>
<header>
    <h1>Ticket Cancellation</h1>
</header>

<main>
    <form action="cancelTicket" method="get">
        <label for="pnr">Enter PNR Number:</label>
        <input type="text" id="pnr" name="pnr" required>
        <button type="submit">Cancel Ticket</button>
    </form>
</main>
<c:if test="${not empty errorMessage}">
    <div class="error-message">
        <p>${errorMessage}</p>
    </div>
</c:if>
<c:if test="${not empty cancellationCharges}">
    <div class="cancellation-charges">
        <h2>Ticket cancelled successfully</h2>
        <p>Cancellation Charges: ${cancellationCharges}</p>
    </div>
</c:if>

</body>
</html>
