<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="org.afzal.servlethibernatedemo.model.Train" %>
<%@ page import="java.util.List" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Book Ticket</title>
    <link rel="stylesheet" href="CSS/book_ticket_style.css">
</head>
<body>
<header>
    <h1>Train Ticket Booking Application</h1>
</header>

<main>
    <h2>Search Trains</h2>
    <form action="book" method="get">
        <label for="source">Source:</label>
        <input type="text" id="source" name="source" required><br>

        <label for="destination">Destination:</label>
        <input type="text" id="destination" name="destination" required><br>

        <label for="journeyDate">Journey Date:</label>
        <input type="date" id="journeyDate" name="journeyDate" required><br>

<%--        <label for="journeyTime">Journey Time:</label>--%>
<%--        <input type="time" id="journeyTime" name="journeyTime" required><br>--%>

        <button type="submit">Fetch Trains</button>
    </form>

    <hr>

        <form action="book" method="post">

            <c:if test="${not empty trains}">
                <h2>Available Trains</h2>
                <%
                List<Train> trainDetails = (List<Train>) request.getAttribute("trains");
                %>
            <table>
                <tr>
                    <th>Train Name</th>
                    <th>Arrival Time</th>
                    <th>Departure Time</th>
                    <th> Ticket Cost Per Head</th>
                    <th>Available Seats</th>
                    <th>Select</th>
                </tr>
                <%for(Train train : trainDetails){%>
                    <tr>
                        <td><%=train.getName()%></td>
                        <%
                            String arrivalTimeString = train.getArrivalTime();
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
                            LocalDateTime arrivalTime = LocalDateTime.parse(arrivalTimeString, formatter);
                            String formattedArrivalTime = arrivalTime.format(DateTimeFormatter.ofPattern("HH:mm"));
                        %>
                        <td><%=formattedArrivalTime%></td>
                        <%
                            String departureTimeString = train.getDepartureTime();
                            LocalDateTime departureTime = LocalDateTime.parse(departureTimeString, formatter);
                            String formattedDepartureTime = departureTime.format(DateTimeFormatter.ofPattern("HH:mm"));
                        %>
                        <td><%=formattedDepartureTime%></td>
                        <td><%=train.getFare()%></td>
                        <td><%=train.getAvailableSeats()%></td>
                        <td><label>
                            <input type="radio" name="trainId" value=<%=train.getId()%>>
                        </label></td>
                    </tr>
                <%}%>
            </table>
                <c:if test="${not empty masterList}">
                    <h2>Co-Passengers</h2>
                </c:if>
            <label for="numberOfPassengers">Number of Passengers:</label>
            <input type="number" id="numberOfPassengers" name="numberOfPassengers" min="1" required><br>
            <c:forEach var="copassenger" items="${masterList}">
<%--                    display copassenger name in checkbox--%>
            <input type="checkbox" name="passengerNames" id="passengerNames" value="${copassenger.name}">
            <label class="checkbox-label" for="passengerNames">${copassenger.name}</label> <br>
            </c:forEach>
            <button type="submit">Book Ticket</button>
        </form>
    </c:if>


    <c:if test="${not empty errorMessage}">
        <p>${errorMessage}</p>
    </c:if>
    <c:if test="${empty trains and not empty errorMessage}">
        <p>No Trains are available</p>
    </c:if>
</main>

<footer>
    <p>&copy; 2024 Train Ticket Booking Application</p>
</footer>
</body>
</html>
