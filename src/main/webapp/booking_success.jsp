<%--
  Created by IntelliJ IDEA.
  User: afzal
  Date: 15/03/24
  Time: 7:39 pm
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Booking Success!!</title>
    <link rel="stylesheet" type="text/css" href="CSS/book_success.css">
</head>
<body>
<%--Get pnr number from servlet--%>
<%
String pnr = (String) request.getParameter("pnr");
%>
<header>
    <h1> Train Ticket Booking Application</h1>
</header>
<%--Display booking success message with PNR number--%>

<div>
    <h1>Booking Success!!</h1>
    <h3>Your PNR number is:<b><%=pnr%></b> </h3>
    <p>Thank you for booking with us!!</p>
    <small>
        Go back to dashboard <a href="dashboard.jsp" >here</a> .
    </small>
</div>


</body>
</html>
