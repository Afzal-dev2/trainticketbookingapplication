<%--
  Created by IntelliJ IDEA.
  User: afzal
  Date: 16/03/24
  Time: 12:05 pm
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dashboard</title>
    <link rel = "stylesheet" href="CSS/dashboard.css">
</head>
<body>
<header>
    <h1>Train Ticket Booking Application</h1>
</header>
<main>
    <div class="action">
        <a href="${pageContext.request.contextPath}/dashboardManager?id=1">Book Ticket</a>
        <a href="${pageContext.request.contextPath}/dashboardManager?id=2">View Ticket</a>
        <a href="${pageContext.request.contextPath}/dashboardManager?id=3">Cancel Ticket</a>
    </div>
</main>
</body>
</html>
