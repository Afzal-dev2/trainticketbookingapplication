package org.afzal.servlethibernatedemo.service;

import org.afzal.servlethibernatedemo.model.Customer;
import org.afzal.servlethibernatedemo.model.MasterList;

import java.util.List;

public interface CustomerService {
    void registerCustomer(Customer customer);
    Customer login(String email, String password);
    void updateCustomer(Customer customer);
    Customer getCustomerById(long customerId);
    Customer getCustomerByName(String customerName);

//    get customer id by name
    long getCustomerIdByName(String customerName);
//    get master list
    List<MasterList> getMasterList(long customerId);
    List<MasterList> getMasterList(String customerName);
    void updateMasterList(List<MasterList> masterList);

}
