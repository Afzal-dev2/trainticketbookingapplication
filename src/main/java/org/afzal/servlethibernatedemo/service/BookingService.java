package org.afzal.servlethibernatedemo.service;

import org.afzal.servlethibernatedemo.model.Booking;
import org.afzal.servlethibernatedemo.model.Train;

public interface BookingService {
    void bookTicket(Booking booking);
    long getPNR(long id);
    Booking getDetailsByPnr(long pnr);
    Booking getBookingWithPassengers(Long bookingId);
    void deleteBooking(Booking booking);
    void updateCoPassengerStatus(Booking booking);
    void updateTrain(Train train);
}