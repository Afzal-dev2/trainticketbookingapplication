package org.afzal.servlethibernatedemo.service;

import org.afzal.servlethibernatedemo.model.Train;

import java.util.List;

public interface TrainService{
    public List<Train> getAllTrains();
    public Train getTrainById(long trainId) ;
    public void updateTrain(Train train) ;
    public List<Train> getTrainsBySourceDestinationDate(String source, String destination, String date);
}
