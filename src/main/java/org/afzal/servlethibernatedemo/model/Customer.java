package org.afzal.servlethibernatedemo.model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "customers")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String mobile;
    private String email;
    private int age;
    private String sex;

//    password field
    private String password;

    public List<MasterList> getMasterList() {
        return masterList;
    }

    public void setMasterList(List<MasterList> masterList) {
        this.masterList = masterList;
    }

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<MasterList> masterList;

    public Customer() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBerthPreference() {
        return berthPreference;
    }

    public void setBerthPreference(String berthPreference) {
        this.berthPreference = berthPreference;
    }


    public double getWallet() {
        return wallet;
    }

    public void setWallet(double wallet) {
        this.wallet = wallet;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    private String berthPreference;

    private double wallet;

    // Constructor
//    generate constructor using  all fields
    public Customer(String name, String mobile, String email, int age, String sex, String berthPreference, double wallet, String password) {
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.age = age;
        this.sex = sex;
        this.berthPreference = berthPreference;
        this.wallet = wallet;
        this.password = password;
        masterList = new ArrayList<>();
    }
}

