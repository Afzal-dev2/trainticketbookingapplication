package org.afzal.servlethibernatedemo.model;


import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "bookings")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long booking_id;

    @Column(name = "customer_id")
    private Long customerId;

    @ManyToOne
    @JoinColumn(name = "train_id")
    private Train train;

    @Column(name = "journey_date")
    private String journeyDate;

    @Column(name = "number_of_passengers")
    private int numberOfPassengers;

    @OneToMany(mappedBy = "booking", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<MasterList> passengers = new ArrayList<>();

    public long getPnr() {
        return pnr;
    }

    public void setPnr(long pnr) {
        this.pnr = pnr;
    }

    //    pnr variable
    @Column (name = "pnr")
    private long pnr;
    // Constructors

    // Getters and setters
    public Long getId() {
        return booking_id;
    }

    public void setId(Long id) {
        this.booking_id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }

    public String getJourneyDate() {
        return journeyDate;
    }

    public void setJourneyDate(String journeyDate) {
        this.journeyDate = journeyDate;
    }

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers(int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }

    public List<MasterList> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<MasterList> passengers) {
        this.passengers = passengers;
    }

//    constructor
    public Booking( Long customerId, Train train, String journeyDate, int numberOfPassengers) {
        this.customerId = customerId;
        this.train = train;
        this.journeyDate = journeyDate;
        this.numberOfPassengers = numberOfPassengers;

    }
    public Booking() {
        // Default constructor
    }
}
