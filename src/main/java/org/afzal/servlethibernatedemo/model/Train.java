package org.afzal.servlethibernatedemo.model;

import jakarta.persistence.*;

@Entity
@Table(name = "train")
public class Train {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String source_station;
    private String destination_station;
    private String departure_time;
    private String arrival_time;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSourceStation() {
        return source_station;
    }

    public void setSourceStation(String sourceStation) {
        this.source_station = sourceStation;
    }

    public String getDestinationStation() {
        return destination_station;
    }

    public void setDestinationStation(String destinationStation) {
        this.destination_station = destinationStation;
    }

    public String getDepartureTime() {
        return departure_time;
    }

    public void setDepartureTime(String departureTime) {
        this.departure_time = departureTime;
    }

    public String getArrivalTime() {
        return arrival_time;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrival_time = arrivalTime;
    }

    public int getAvailableSeats() {
        return available_seats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.available_seats = availableSeats;
    }

    public double getFare() {
        return fare;
    }

    public void setFare(double fare) {
        this.fare = fare;
    }

    private int available_seats;
    private double fare;

    // Constructors, getters, and setters
}

