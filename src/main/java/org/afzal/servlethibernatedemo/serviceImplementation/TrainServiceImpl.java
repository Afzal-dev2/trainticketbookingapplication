package org.afzal.servlethibernatedemo.serviceImplementation;

import org.afzal.servlethibernatedemo.DAO.TrainDAO;
import org.afzal.servlethibernatedemo.service.TrainService;
import org.afzal.servlethibernatedemo.model.Train;

import java.util.List;

public class TrainServiceImpl implements TrainService {
    private final TrainDAO trainDAO;
    public TrainServiceImpl(TrainDAO trainDAO) {
        this.trainDAO = trainDAO;
    }
    @Override
    public List<Train> getAllTrains() {
        return trainDAO.getAllTrains();
    }
    @Override
    public Train getTrainById(long trainId) {
        return trainDAO.getTrainById(trainId);
    }
    @Override
    public void updateTrain(Train train) {
        trainDAO.updateTrain(train);
    }
    @Override
    public List<Train> getTrainsBySourceDestinationDate(String source, String destination, String date) {
        return trainDAO.getTrainsBySourceDestinationDate(source, destination, date);
    }

}
