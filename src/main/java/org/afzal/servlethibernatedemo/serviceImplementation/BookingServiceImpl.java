package org.afzal.servlethibernatedemo.serviceImplementation;

import org.afzal.servlethibernatedemo.DAO.BookingDAO;
import org.afzal.servlethibernatedemo.service.BookingService;
import org.afzal.servlethibernatedemo.model.Booking;

public class BookingServiceImpl implements BookingService {
    private final BookingDAO bookingDAO;
    public BookingServiceImpl(BookingDAO bookingDAO) {
        this.bookingDAO = bookingDAO;
    }
    @Override
    public void bookTicket(Booking booking) {
        bookingDAO.save(booking);
    }
    @Override
    public long getPNR(long id) {
        return bookingDAO.getPNR(id);
    }
    @Override
    public Booking getDetailsByPnr(long pnr) {
        return bookingDAO.getDetailsByPnr(pnr);

    }
    @Override
    public Booking getBookingWithPassengers(Long bookingId) {
        return bookingDAO.getBookingWithPassengers(bookingId);
    }
    @Override
    public void deleteBooking(Booking booking) {
        bookingDAO.delete(booking);
    }
    @Override
    public void updateCoPassengerStatus(Booking booking) {
        bookingDAO.updateCoPassengerStatus(booking);
    }
    @Override
    public void updateTrain(org.afzal.servlethibernatedemo.model.Train train) {
        bookingDAO.updateTrain(train);
    }
}
