package org.afzal.servlethibernatedemo.serviceImplementation;

import org.afzal.servlethibernatedemo.DAO.CustomerDAO;
import org.afzal.servlethibernatedemo.DAOImplementation.CustomerDAOImpl;
import org.afzal.servlethibernatedemo.model.MasterList;
import org.afzal.servlethibernatedemo.service.CustomerService;
import org.afzal.servlethibernatedemo.model.Customer;

import java.util.List;

public class CustomerServiceImpl implements CustomerService {
    private final CustomerDAO customerDao = new CustomerDAOImpl();

    @Override
    public void registerCustomer(Customer customer) {
        customerDao.saveCustomer(customer);
    }
    @Override
    public Customer login(String email, String password) {
        return customerDao.getCustomerByEmailAndPassword(email, password);
    }
    @Override
    public void updateCustomer(Customer customer) {
        customerDao.update(customer);
    }

    @Override
    public Customer getCustomerById(long customerId) {
        return customerDao.getCustomerById(customerId);
    }

    @Override
    public List<MasterList> getMasterList(long customerId) {
        return customerDao.getMasterList(customerId);
    }
    @Override
    public List<MasterList> getMasterList(String customerName) {
        return customerDao.getMasterList(customerName);
    }
    @Override
    public Customer getCustomerByName(String customerName) {
        return customerDao.getCustomerByName(customerName);
    }
    @Override
    public long getCustomerIdByName(String customerName) {
        return customerDao.getCustomerIdByName(customerName);
    }

    @Override
    public void updateMasterList(List<MasterList> masterList) {
        customerDao.updateMasterList(masterList);
    }
}
