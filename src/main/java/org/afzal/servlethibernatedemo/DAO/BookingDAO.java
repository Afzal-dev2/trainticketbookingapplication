package org.afzal.servlethibernatedemo.DAO;

import org.afzal.servlethibernatedemo.model.Booking;
import org.afzal.servlethibernatedemo.model.Train;

public interface BookingDAO {
    void save(Booking booking);
    Booking getBookingById(long bookingId);
    long getPNR(long id);
    Booking getDetailsByPnr(long pnr);
    Booking getBookingWithPassengers(Long bookingId);
    void delete(Booking booking);
    void updateCoPassengerStatus(Booking booking);
    void updateTrain(Train train);
}
