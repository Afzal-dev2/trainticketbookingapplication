package org.afzal.servlethibernatedemo.DAO;

import org.afzal.servlethibernatedemo.model.Customer;
import org.afzal.servlethibernatedemo.model.MasterList;

import java.util.List;

public interface CustomerDAO {
    void saveCustomer(Customer customer);

    public Customer getCustomerByEmailAndPassword(String email, String password);

    public Customer getCustomerByName(String customerName);

    void update(Customer customer);

    Customer getCustomerById(long customerId);

    long getCustomerIdByName(String customerName);

    public List<MasterList> getMasterList(long customerId);

    public List<MasterList> getMasterList(String customerName);

    public void updateMasterList(List<MasterList> masterList);
}
