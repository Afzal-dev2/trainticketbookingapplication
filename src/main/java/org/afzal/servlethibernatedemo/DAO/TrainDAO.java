package org.afzal.servlethibernatedemo.DAO;

import org.afzal.servlethibernatedemo.model.Train;

import java.util.List;

public interface TrainDAO {
    List<Train> getAllTrains();
    Train getTrainById(long trainId);
    void updateTrain(Train train);
    List<Train> getTrainsBySourceDestinationDate(String source, String destination, String date);
}
