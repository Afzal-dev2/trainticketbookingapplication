package org.afzal.servlethibernatedemo.DAOImplementation;

import org.afzal.servlethibernatedemo.DAO.CustomerDAO;
import org.afzal.servlethibernatedemo.HibernateUtil;
import org.afzal.servlethibernatedemo.model.Customer;
import org.afzal.servlethibernatedemo.model.MasterList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;


public class CustomerDAOImpl implements CustomerDAO {
    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    @Override
    public void saveCustomer(Customer customer) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(customer);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    @Override
    public Customer getCustomerByEmailAndPassword(String email, String password) {
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("FROM Customer WHERE email = :email AND password = :password", Customer.class);
            query.setParameter("email", email);
            query.setParameter("password", password);
            return query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Customer getCustomerByName(String customerName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("FROM Customer WHERE name = :customerName", Customer.class);
            query.setParameter("customerName", customerName);
            return query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void update(Customer customer) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(customer);
            transaction.commit();
        }
    }

    @Override
    public Customer getCustomerById(long customerId) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Customer.class, customerId);
        }
    }

    @Override
    public long getCustomerIdByName(String customerName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("FROM Customer WHERE name = :customerName", Customer.class);
            query.setParameter("customerName", customerName);
            return query.uniqueResult().getId();
        }
    }

    @Override
    public List<MasterList> getMasterList(long customerId) {
        try (Session session = sessionFactory.openSession()) {
            Query<MasterList> query = session.createQuery("FROM MasterList WHERE customer.id = :customerId", MasterList.class);
            query.setParameter("customerId", customerId);
            return query.list();
        }
    }
    @Override
    public List<MasterList> getMasterList(String customerName) {
        try (Session session = sessionFactory.openSession()) {
            Query<MasterList> query = session.createQuery("FROM MasterList WHERE customer.name = :customerName", MasterList.class);
            query.setParameter("customerName", customerName);
            return query.list();
        }
    }
    @Override
    public void updateMasterList(List<MasterList> masterList) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            for (MasterList master : masterList) {
                System.out.println(master.getBooking().getId() + " " + master.getId());
                session.update(master);
            }
            transaction.commit();
        }
    }

    /*@Override
    public void updateMasterList(List<MasterList> masterList) {
//       use HQL
        try (Session session = sessionFactory.openSession()) {
//            below query is not working
//            Don't use result set in update query

            for (MasterList masterList1 : masterList) {
//                query.setParameter("bookingId", masterList1.getBooking().getId());
//                query.setParameter("id", masterList1.getId());
//                query.executeUpdate();
                session.createQuery("UPDATE MasterList SET booking.id = " + masterList1.getBooking().getId() + " WHERE id = " + masterList1.getId(), MasterList.class).executeUpdate()~;
            }
        }
    }
*/

}
