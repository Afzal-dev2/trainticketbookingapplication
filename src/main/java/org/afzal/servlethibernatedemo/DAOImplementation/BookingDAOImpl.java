package org.afzal.servlethibernatedemo.DAOImplementation;

import org.afzal.servlethibernatedemo.DAO.BookingDAO;
import org.afzal.servlethibernatedemo.model.Booking;
import org.afzal.servlethibernatedemo.model.Train;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class BookingDAOImpl implements BookingDAO {

    private final SessionFactory sessionFactory;

    public BookingDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(Booking booking) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(booking);
            transaction.commit();
        }
    }
//    get booking by id
    @Override
    public Booking getBookingById(long bookingId) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Booking.class, bookingId);
        }
    }
//    get pnr by id
    @Override
    public long getPNR(long id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Booking.class, id).getPnr();
        }
    }
//    get details by pnr
    @Override
    public Booking getDetailsByPnr(long pnr) {
        try (Session session = sessionFactory.openSession()) {
//            use HQL to get the booking details
            return session.createQuery("from Booking where pnr = :pnr", Booking.class)
                    .setParameter("pnr", pnr)
                    .uniqueResult();
        }
    }
    @Override
    public Booking getBookingWithPassengers(Long bookingId) {
        try (Session session = sessionFactory.openSession()) {
            String jpql = "SELECT b FROM Booking b LEFT JOIN FETCH b.passengers WHERE b.id = :bookingId";
            return session.createQuery(jpql, Booking.class)
                    .setParameter("bookingId", bookingId)
                    .getSingleResult();
        }
    }
    @Override
    public void delete(Booking booking) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(booking);
            transaction.commit();
        }
    }
//    update co-passenger's booking id to null
    @Override
    public void updateCoPassengerStatus(Booking booking) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            long bookingId = booking.getId();
            session.createQuery("UPDATE MasterList SET booking.id = null WHERE booking.id = :bookingId")
                    .setParameter("bookingId", bookingId)
                    .executeUpdate();
            transaction.commit();
        }
    }
//    update train's available seats
    @Override
    public void updateTrain(Train train) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(train);
            transaction.commit();
        }
    }
}