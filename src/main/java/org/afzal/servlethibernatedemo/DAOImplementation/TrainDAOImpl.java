package org.afzal.servlethibernatedemo.DAOImplementation;

import org.afzal.servlethibernatedemo.DAO.TrainDAO;
import org.afzal.servlethibernatedemo.model.Train;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class TrainDAOImpl implements TrainDAO {

    private final SessionFactory sessionFactory;

    public TrainDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Train> getAllTrains() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Train", Train.class).list();
        }
    }
    @Override
    public Train getTrainById(long trainId) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Train.class, trainId);
        }
    }

    @Override
    public void updateTrain(Train train) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(train);
            transaction.commit();
        }
    }
    @Override
    public List<Train> getTrainsBySourceDestinationDate(String source, String destination, String date) {
//        convert date to YYYY-MM-DDTHH:MM:SS format
//        get current time
        String time  = java.time.LocalTime.now().toString();
        date = date + time.substring(2,8);
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Train WHERE source_station = :source AND destination_station = :destination", Train.class)
                    .setParameter("source", source)
                    .setParameter("destination", destination)
                    .list();
        }
    }

}
