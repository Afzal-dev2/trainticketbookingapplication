package org.afzal.servlethibernatedemo.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.afzal.servlethibernatedemo.DAOImplementation.BookingDAOImpl;
import org.afzal.servlethibernatedemo.HibernateUtil;
import org.afzal.servlethibernatedemo.model.Booking;
import org.afzal.servlethibernatedemo.model.Customer;
import org.afzal.servlethibernatedemo.model.Train;
import org.afzal.servlethibernatedemo.service.BookingService;
import org.afzal.servlethibernatedemo.service.CustomerService;
import org.afzal.servlethibernatedemo.serviceImplementation.BookingServiceImpl;
import org.afzal.servlethibernatedemo.serviceImplementation.CustomerServiceImpl;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;

@WebServlet("/cancelTicket")
public class TicketCancellationServlet extends HttpServlet {
//    implement doGet method and redirect the cancelticket.jsp
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Receive booking ID or PNR number from the user
        String pnr = request.getParameter("pnr");
        // Validate the PNR number
        if (pnr == null || pnr.isEmpty()) {
            request.setAttribute("errorMessage", "PNR number is required.");
            request.getRequestDispatcher("cancel_ticket.jsp").forward(request, response);
            return;
        }
        long pnrNumber;
        try {
            pnrNumber = Long.parseLong(pnr);
        } catch (NumberFormatException e) {
            request.setAttribute("errorMessage", "Invalid PNR number format.");
            request.getRequestDispatcher("cancel_ticket.jsp").forward(request, response);
            return;
        }
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        // Fetch booking details from the database based on booking ID or PNR number
        BookingService bookingService = new BookingServiceImpl(new BookingDAOImpl(sessionFactory));
        Booking booking;
        try{
            booking = bookingService.getDetailsByPnr(pnrNumber);
            if (booking == null) {
                request.setAttribute("errorMessage", "Booking not found for the provided PNR number.");
                request.getRequestDispatcher("cancel_ticket.jsp").forward(request, response);
                return;
        }
        Train selectedTrain = booking.getTrain();
        // Calculate cancellation charges
        double cancellationCharges = calculateCancellationCharges(booking, request, response);

        // Deduct cancellation charges from customer's wallet balance
        CustomerService customerService = new CustomerServiceImpl();
        Customer customer = customerService.getCustomerById(booking.getCustomerId()); // Update this based on your implementation
        double remainingBalance = customer.getWallet() - cancellationCharges;
//        check if the remaining balance is less than 0
        if (remainingBalance < 0) {
            request.setAttribute("errorMessage", "Insufficient Balance");
            request.getRequestDispatcher("cancel_ticket.jsp").forward(request, response);
            return;
        }
        customer.setWallet(remainingBalance);
        customerService.updateCustomer(customer);

//        update the seats in the train
        selectedTrain.setAvailableSeats(selectedTrain.getAvailableSeats() + booking.getNumberOfPassengers());
        bookingService.updateTrain(selectedTrain);
        //update the co-passengers booking status to NULL
        bookingService.updateCoPassengerStatus(booking);
        //delete booking entry from the database
        bookingService.deleteBooking(booking);
        System.out.println("Booking cancelled successfully");
        System.out.println("Cancellation charges: " + cancellationCharges);
        // Display confirmation message to the user
        request.setAttribute("cancellationCharges", cancellationCharges);
        request.getRequestDispatcher("cancel_ticket.jsp").forward(request, response);
//        request.getRequestDispatcher("cancel_ticket.jsp").forward(request, response);
    }
        catch (Exception e) {
            request.setAttribute("errorMessage", "Invalid PNR number.");
            request.getRequestDispatcher("cancel_ticket.jsp").forward(request, response);
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

    private double calculateCancellationCharges(Booking booking, HttpServletRequest request, HttpServletResponse response) {
        double cancellationCharges = 0.0;
        // Get current time
        LocalDateTime currentTime = LocalDateTime.now();
        String departureTimeString = booking.getJourneyDate();
        System.out.println(departureTimeString);
        LocalDateTime departureTime = LocalDateTime.parse(departureTimeString);

        // Check if cancellation time is after departure time
        if (currentTime.isAfter(departureTime)) {
            System.out.println("Train has already departed. Cancellation is not allowed.");
            request.setAttribute("errorMessage","Train has already departed. Cancellation is not allowed.");
            try {
                request.getRequestDispatcher("cancel_ticket.jsp").forward(request, response);
            } catch (ServletException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        // Calculate time difference in hours between current time and departure time
        long hoursDifference = Duration.between(currentTime, departureTime).toHours();

        // Get fare for the booking
        double fare = booking.getTrain().getFare();
        int numberOfPassengers = booking.getNumberOfPassengers()+1;

        // Determine cancellation charges based on time difference
        if (hoursDifference > 48) {
            // More than 48 hours before departure
            cancellationCharges = (numberOfPassengers * 120.0); // Flat cancellation charge Rs. 180 per passenger
        } else if (hoursDifference <= 48 && hoursDifference > 12) {
            // Between 48 and 12 hours before departure
            cancellationCharges = (0.25 * fare * numberOfPassengers); // 25% of fare
        } else if (hoursDifference <= 12 && hoursDifference >= 4) {
            // Between 12 and 4 hours before departure
            cancellationCharges = (0.5 * fare * numberOfPassengers); // 50% of fare
        } else {
            // Less than 4 hours before departure

            cancellationCharges = (0.5 * fare * numberOfPassengers); // 50% of fare
        }

        return cancellationCharges;
    }

}

