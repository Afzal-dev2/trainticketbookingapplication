package org.afzal.servlethibernatedemo.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.afzal.servlethibernatedemo.DAOImplementation.BookingDAOImpl;
import org.afzal.servlethibernatedemo.HibernateUtil;
import org.afzal.servlethibernatedemo.model.Booking;
import org.afzal.servlethibernatedemo.model.Customer;
import org.afzal.servlethibernatedemo.model.MasterList;
import org.afzal.servlethibernatedemo.service.BookingService;
import org.afzal.servlethibernatedemo.service.CustomerService;
import org.afzal.servlethibernatedemo.serviceImplementation.BookingServiceImpl;
import org.afzal.servlethibernatedemo.serviceImplementation.CustomerServiceImpl;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.util.List;

@WebServlet("/view")
public class ViewTicketServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Get the PNR number from the request parameter
        String pnr = request.getParameter("pnr");

        // Validate the PNR number
        if (pnr == null || pnr.isEmpty()) {
            request.setAttribute("errorMessage", "PNR number is required.");
            request.getRequestDispatcher("view_ticket.jsp").forward(request, response);
            return;
        }

        // Parse the PNR number to long
        long pnrNumber;
        try {
            pnrNumber = Long.parseLong(pnr);
        } catch (NumberFormatException e) {
            request.setAttribute("errorMessage", "Invalid PNR number format.");
            request.getRequestDispatcher("view_ticket.jsp").forward(request, response);
            return;
        }
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        // Fetch booking details corresponding to the provided PNR number
        BookingService bookingService = new BookingServiceImpl(new BookingDAOImpl(sessionFactory));
       try{
           Booking bookingDetails = bookingService.getDetailsByPnr(pnrNumber);
//    print the booking details
           System.out.println(bookingDetails.getPnr());

           CustomerService customerService = new CustomerServiceImpl();
           Customer customerData = customerService.getCustomerById(bookingDetails.getCustomerId());

           Booking bookingWithPassengers = bookingService.getBookingWithPassengers(bookingDetails.getId());
           // Pass booking details and PNR number as request attributes
           request.setAttribute("bookingDetails", bookingDetails);
           request.setAttribute("bookingPassengers", bookingWithPassengers.getPassengers());
              request.setAttribute("customerData", customerData);
           request.getRequestDispatcher("view_ticket.jsp").forward(request, response);
       }
         catch (Exception e) {
             request.setAttribute("errorMessage", "Invalid PNR number.");
             request.getRequestDispatcher("view_ticket.jsp").forward(request, response);
         }


    }
}
