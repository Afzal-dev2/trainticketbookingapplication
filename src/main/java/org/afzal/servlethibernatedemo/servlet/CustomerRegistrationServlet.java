package org.afzal.servlethibernatedemo.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import org.afzal.servlethibernatedemo.model.MasterList;
import org.afzal.servlethibernatedemo.serviceImplementation.CustomerServiceImpl;
import org.afzal.servlethibernatedemo.service.CustomerService;
import org.afzal.servlethibernatedemo.model.Customer;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/register")
public class CustomerRegistrationServlet extends HttpServlet {

    private final CustomerService customerService = new CustomerServiceImpl();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Retrieve form data
        String name = request.getParameter("name");
        String mobile = request.getParameter("mobile");
        String email = request.getParameter("email");
        int age = Integer.parseInt(request.getParameter("age"));
        String sex = request.getParameter("sex");
        String berthPreference = request.getParameter("berth");
        double wallet = Double.parseDouble(request.getParameter("wallet"));
        String password = request.getParameter("password");
        int passengerCount = Integer.parseInt(request.getParameter("passenger_count"));

        // Create Customer object
        Customer customer = new Customer(name, mobile, email, age, sex, berthPreference, wallet, password);

        for (int i = 1; i <= passengerCount; i++) {
            String passengerName = request.getParameter("passengerName" + i);
            String passengerMobile = request.getParameter("passengerMobile" + i);
            String passengerEmail = request.getParameter("passengerEmail" + i);
            int passengerAge = Integer.parseInt(request.getParameter("passengerAge" + i));
            String passengerBerth = request.getParameter("passengerBerth" + i);
            String passengerGender = request.getParameter("passengerSex" + i);

            MasterList master = new MasterList();
            master.setName(passengerName);
            master.setMobileNumber(passengerMobile);
            master.setEmail(passengerEmail);
            master.setAge(passengerAge);
            master.setBerthPreference(passengerBerth);
            master.setSex(passengerGender);
            master.setBerthPreference(passengerBerth);
            master.setCustomer(customer);

            customer.getMasterList().add(master);
        }


        // Save customer to database
        customerService.registerCustomer(customer);

//        store customer object in session
        HttpSession session = request.getSession();
        session.setAttribute("customerName", customer);
        Cookie cookie = new Cookie("customerName",  URLEncoder.encode(customer.getName(), StandardCharsets.UTF_8));
        cookie.setMaxAge(30 * 24 * 60 * 60); // Cookie lasts for 30 days
        response.addCookie(cookie);
        // Redirect to a success page
        response.sendRedirect("registration_success.html");
    }
}
