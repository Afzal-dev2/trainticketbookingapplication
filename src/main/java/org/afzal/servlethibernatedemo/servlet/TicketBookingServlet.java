package org.afzal.servlethibernatedemo.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import org.afzal.servlethibernatedemo.DAOImplementation.BookingDAOImpl;
import org.afzal.servlethibernatedemo.DAOImplementation.TrainDAOImpl;
import org.afzal.servlethibernatedemo.HibernateUtil;
import org.afzal.servlethibernatedemo.model.MasterList;
import org.afzal.servlethibernatedemo.service.BookingService;
import org.afzal.servlethibernatedemo.serviceImplementation.BookingServiceImpl;
import org.afzal.servlethibernatedemo.serviceImplementation.CustomerServiceImpl;
import org.afzal.servlethibernatedemo.serviceImplementation.TrainServiceImpl;
import org.afzal.servlethibernatedemo.service.CustomerService;
import org.afzal.servlethibernatedemo.service.TrainService;
import org.afzal.servlethibernatedemo.model.Booking;
import org.afzal.servlethibernatedemo.model.Customer;
import org.afzal.servlethibernatedemo.model.Train;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/book")
public class TicketBookingServlet extends HttpServlet {

    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    private final TrainService trainService = new TrainServiceImpl(new TrainDAOImpl(sessionFactory));
    private final CustomerService customerService = new CustomerServiceImpl();
    BookingService bookingService = new BookingServiceImpl(new BookingDAOImpl(sessionFactory));

    HttpSession session;
    String customerName;

    Customer customer;

    List<MasterList> masterList;

    String source;
    String destination;
    String date;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("source") == null || request.getParameter("destination") == null || request.getParameter("journeyDate") == null) {
            System.out.println("One of the provided parameters is null");
            response.sendRedirect("book_ticket.jsp");
            return;
        }
         session = request.getSession();
         customer =  session.getAttribute("customerName") != null ? (Customer) session.getAttribute("customerName") : null;
        if (customer != null) {
            customerName = customer.getName();
        }
//        else retrieve customer from cookie
        else {
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("customerName")) {
//                        using coookies value to fetch customer from database
                        System.out.println("Cookie found - Using cookie value to fetch customer from database");
                        customerName = cookie.getValue();
                        break;
                    }
                }
            }
            //        fetch customer from database
            customer = customerService.getCustomerByName(customerName);
        }
//    receive source, destination and date from request parameter
         source = request.getParameter("source");
         destination = request.getParameter("destination");
         date = request.getParameter("journeyDate");


// Now customerName contains the customer's name

//        List<Train> trains = trainService.getAllTrains();
//        get customers from masterlist
        masterList = customerService.getMasterList(customerName);
//        System.out.println(trains.size());
        System.out.println(masterList.size());
        if (source != null && destination != null && date != null) {
            List<Train> trains = trainService.getTrainsBySourceDestinationDate(source, destination, date);
             System.out.println("Train size:    " + trains.size());
            request.setAttribute("trains", trains);
            request.setAttribute("masterList", masterList);
            request.getRequestDispatcher("book_ticket.jsp").forward(request, response);
            return;
        }
//        request.setAttribute("trains", trains);
//        request.setAttribute("masterList", masterList);
//        request.getRequestDispatcher("book_ticket.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        long trainId = Long.parseLong(request.getParameter("trainId"));

        int numberOfPassengers = Integer.parseInt(request.getParameter("numberOfPassengers"));

//        get passengers ids from request parameter and convert to int
        String[] passengerNames = request.getParameterValues("passengerNames");
        System.out.println("PAssenger names: " + passengerNames.length);
        List<MasterList> passengersToBeAdded = new ArrayList<>();

        Train selectedTrain = trainService.getTrainById(trainId);
        Customer customer = customerService.getCustomerByName(customerName);
        double totalFare = selectedTrain.getFare() * (numberOfPassengers + 1); // +1 for the customer
//
//        if (customer == null || selectedTrain == null || totalFare <= 0 || numberOfPassengers <= 0 || passengerNames == null || passengerNames.length != numberOfPassengers) {
//            response.sendRedirect("error.jsp");
//            return;
//        }

        double walletBalance = customer.getWallet();
        if (walletBalance < totalFare) {
            request.setAttribute("errorMessage", "Insufficient Balance to Book Ticket. Please recharge your wallet.");
            request.getRequestDispatcher("book_ticket.jsp").forward(request, response);
            return;
        }

        customer.setWallet(walletBalance - totalFare);
        customerService.updateCustomer(customer);

        selectedTrain.setAvailableSeats(selectedTrain.getAvailableSeats() - numberOfPassengers);
        trainService.updateTrain(selectedTrain);

//        get current time in HH:MM:SS format
        String time = java.time.LocalTime.now().toString();
        date = date + "T" + time.substring(0, 8);
//        check if time is greater than departure time
//        if (java.time.LocalTime.now().isAfter(java.time.LocalTime.parse(selectedTrain.getDepartureTime()))) {
//            request.setAttribute("errorMessage", "Train has already departed. Cannot book ticket for this train.");
//            request.getRequestDispatcher("book_ticket.jsp").forward(request, response);
//            return;
//        }
        Booking booking = new Booking(
                customer.getId(),
                selectedTrain,
                date,
                numberOfPassengers
        );

        for (MasterList masterList : masterList) {
            System.out.println("MasterList: " + masterList.getName());
            for (String passengerName : passengerNames) {
                System.out.println("Passenger: " + passengerName);
                if (masterList.getName().equals(passengerName)) {
                    System.out.println("Matched");
                    masterList.setBooking(booking);
                    booking.getPassengers().add(masterList);
                    passengersToBeAdded.add(masterList);
                }
            }
        }


//        generate random 10 digit number as PNR
        long pnr = (long) (Math.random() * 10000000000L);
        booking.setPnr(pnr);

//        update masterlist
//        print passengers to be added
        for (MasterList masterList : passengersToBeAdded) {
            System.out.println(masterList.getName());
        }
        bookingService.bookTicket(booking);
        customerService.updateMasterList(passengersToBeAdded);


//        use pnr to redirect to booking success page

        response.sendRedirect("booking_success.jsp?pnr=" + booking.getPnr());
    }

}
