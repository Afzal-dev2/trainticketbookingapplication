package org.afzal.servlethibernatedemo.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet("/dashboardManager")
public class DashboardManagerServlet extends HttpServlet {
// check the id and redirect to the appropriate page
// if id is 1, redirect to book servlet
// if id is 2, redirect to view servlet
// if id is 3, redirect to cancel servlet
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));
    if (id == 1) {
        response.sendRedirect("book");
    } else if (id == 2) {
        response.sendRedirect("view");
    } else if (id == 3) {
        response.sendRedirect("cancelTicket");
    }
}
}
