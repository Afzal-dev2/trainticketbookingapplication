package org.afzal.servlethibernatedemo.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.afzal.servlethibernatedemo.serviceImplementation.CustomerServiceImpl;
import org.afzal.servlethibernatedemo.service.CustomerService;
import org.afzal.servlethibernatedemo.model.Customer;

import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private final CustomerService customerService = new CustomerServiceImpl();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Retrieve form data
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        // Validate user credentials
        Customer customer = customerService.login(email, password);
        if (customer != null) {
            // Redirect to dashboard or home page
            response.sendRedirect("dashboard.jsp");
        } else {
            // Redirect back to login page with error message
            response.sendRedirect("index.html?error=1");
        }
    }
}